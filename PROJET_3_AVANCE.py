import csv
from math import sqrt

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    tableau_perso= [{Key : value.replace('\xa0', ' ') for Key, value in element.items()}for element in reader]



with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    tableau_carac = [{Key : value.replace('\xa0', ' ') for Key, value in element.items()}for element in reader]

poudlard_perso = []

for poudlard_character in tableau_carac:
    for kaggle_character in tableau_perso:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_perso.append(poudlard_character)

profils_a_tester = [{'Courage' : '9', 'Ambition' : '2', 'Intelligence' : '8', 'Good' : '9'},
{'Courage' : '6', 'Ambition' : '7', 'Intelligence' : '9', 'Good' : '7'},
{'Courage' : '3', 'Ambition' : '8', 'Intelligence' : '6', 'Good' : '3'},
{'Courage' : '2', 'Ambition' : '3', 'Intelligence' : '7', 'Good' : '8'},
{'Courage' : '3', 'Ambition' : '4', 'Intelligence' : '8', 'Good' : '8'}]

def distance(joueur1, joueur2):
    return sqrt((int(joueur1['Courage']) - int(joueur2['Courage'])) ** 2
                + (int(joueur1['Ambition']) - int(joueur2['Ambition'])) ** 2 
                + (int(joueur1['Intelligence']) - int(joueur2['Intelligence'])) ** 2
                + (int(joueur1['Good']) - int(joueur2['Good']))** 2)



def neight(perso):
    for element in profils_a_tester:    
        perso.sort(key=lambda x: distance(element, x))
        poudlard_kppv = perso[:5]
        return poudlard_kppv
        
kppv = neight(poudlard_perso)
print(kppv)

def house_prediction(neightboor):
    house_set = {"Gryffindor": 0, "Ravenclaw" : 0, "Hufflepuff" : 0, "Slytherin":0 }
    for house in kppv:
        house_set[house["House"]] += 1
    maison_value = list(house_set.items())
    maison_value.sort(key = lambda x: x[1], reverse=True)
    print(maison_value[:1])
    #sorted_house = maison_value[:1]
    #return sorted_house[:5]
        


print(house_prediction(kppv))


