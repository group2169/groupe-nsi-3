# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 13:12:27 2022

@author: DECUREY
"""

import csv
from math import sqrt

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    tableau_perso= [{Key : value.replace('\xa0', ' ') for Key, value in element.items()}for element in reader]



with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    tableau_carac = [{Key : value.replace('\xa0', ' ') for Key, value in element.items()}for element in reader]

poudlard_perso = []

for poudlard_character in tableau_carac:
    for kaggle_character in tableau_perso:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_perso.append(poudlard_character)

profils_a_tester = [{'Courage' : '9', 'Ambition' : '2', 'Intelligence' : '8', 'Good' : '9'},
{'Courage' : '6', 'Ambition' : '7', 'Intelligence' : '9', 'Good' : '7'},
{'Courage' : '3', 'Ambition' : '8', 'Intelligence' : '6', 'Good' : '3'},
{'Courage' : '2', 'Ambition' : '3', 'Intelligence' : '7', 'Good' : '8'},
{'Courage' : '3', 'Ambition' : '4', 'Intelligence' : '8', 'Good' : '8'}]

def distance(joueur1, joueur2):
    """
    Calcule de la distance des K plus proches voisins en fonction du courage,
    de l'ambition, de l'intelligence et de good.
    ----------
    Entrée:Liste de disctionnaire
    Sortie:La racine carré du carré des sommes des caractéristiques
    ---------- 
    """
    return sqrt((int(joueur1['Courage']) - int(joueur2['Courage'])) ** 2
                + (int(joueur1['Ambition']) - int(joueur2['Ambition'])) ** 2 
                + (int(joueur1['Intelligence']) - int(joueur2['Intelligence'])) ** 2
                + (int(joueur1['Good']) - int(joueur2['Good']))** 2)



def neight(perso, character_test):
    '''
    Trie des distances par ordre croissant, et cela prend les 5 plus petites
    ----------
    Entrée:Listes des distances entre les personnages
    Sortie:Listes des 5 plus petites distances
    ----------
    '''
    perso.sort(key=lambda x: distance(character_test, x))
    poudlard_kppv = perso[:5]
    return poudlard_kppv
kppv = []
for perso_inconnu in profils_a_tester:
    kppv.append(neight(poudlard_perso, perso_inconnu))
    
def house_prediction(k_plus_proche):
    '''
    Ajouter les maisons des K dans un dictionnaires
    ----------
    Entrée:Dictionnaires des K plus proches voisins
    Sortie:Maison des K plus proches voisins
    ---------
    '''
    house_set = {"Gryffindor": 0, "Ravenclaw" : 0, "Hufflepuff" : 0, "Slytherin":0 }
    
    for house in k_plus_proche:
        house_set[house["House"]] += 1
    maison_value = list(house_set.items())
    maison_value.sort(key = lambda x: x[1], reverse=True)
    return maison_value[:1]

definitive_house = []
for house_kppv in kppv:
    definitive_house.append(house_prediction(house_kppv))
for i in range(5):

    print(f"La maison du {i+1}er/ème profil est {definitive_house[i]}")
    

