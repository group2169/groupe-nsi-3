# -*- coding: utf-8 -*-
"""
Mini projet  3, le choixpeau magique.
Eleves ayant participer au projet: 
    Auguste MANCHEC 106
    Nolann DECUREY 106
    Titouan PLU 106

Nous avons choisis le bonus où on peut changer la valeur de k pour
avoir le nombre voulu de voisin.
"""

import csv
from math import sqrt

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_perso= [{Key : value.replace('\xa0', ' ') for Key, value in element.items()}for element in reader]



with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_carac = [{Key : value.replace('\xa0', ' ') for Key, value in element.items()}for element in reader]

poudlard_perso = []

for poudlard_character in table_carac:
    for fusion_character in table_perso:
        if poudlard_character['Name'] == fusion_character['Name']:
            poudlard_character.update(fusion_character)
            poudlard_perso.append(poudlard_character)

profils_a_tester = [{'Courage' : '9', 'Ambition' : '2', 'Intelligence' : '8', 'Good' : '9'},
{'Courage' : '6', 'Ambition' : '7', 'Intelligence' : '9', 'Good' : '7'},
{'Courage' : '3', 'Ambition' : '8', 'Intelligence' : '6', 'Good' : '3'},
{'Courage' : '2', 'Ambition' : '3', 'Intelligence' : '7', 'Good' : '8'},
{'Courage' : '3', 'Ambition' : '4', 'Intelligence' : '8', 'Good' : '8'}]

def distance(character1, character2):
    """
    Calcule de la distance des K plus proches voisins en fonction du courage,
    de l'ambition, de l'intelligence et de good.
    ----------
    Entrée:Liste de disctionnaire
    Sortie:La racine carré du carré des sommes des caractéristiques
    ---------- 
    """

    return sqrt((int(character1['Courage']) - int(character2['Courage'])) ** 2
                + (int(character1['Ambition']) - int(character2['Ambition'])) ** 2 
                + (int(character1['Intelligence']) - int(character2['Intelligence'])) ** 2
                + (int(character1['Good']) - int(character2['Good']))** 2)

value_k_choice = 5

def neight(perso, character_test, choice): 
    '''
    Trie des distances par ordre croissant, et cela prend les 5 plus petites
    ----------
    Entrée:Listes des distances entre les personnages
    Sortie:Listes des 5 plus petites distances
    ----------
    '''

    perso.sort(key=lambda x: distance(character_test, x))
    poudlard_kppv = perso[:choice]
    return poudlard_kppv
kppv = []
for perso_inconnu in profils_a_tester:
    kppv.append(neight(poudlard_perso, perso_inconnu, value_k_choice))
    
def house_prediction(k_plus_proche):
    '''
    Ajouter les maisons des K dans un dictionnaires
    ----------
    Entrée:Dictionnaires des K plus proches voisins
    Sortie:Maison des K plus proches voisins
    ---------
    '''

    house_set = {"Gryffindor": 0, "Ravenclaw" : 0, "Hufflepuff" : 0, "Slytherin":0 }
    
    for house in k_plus_proche:
        house_set[house["House"]] += 1
    maison_value = list(house_set.items())
    maison_value.sort(key = lambda x: x[1], reverse=True)
    return maison_value[:1]

definitive_house = []
for house_kppv in kppv:
    definitive_house.append(house_prediction(house_kppv))

for i in range(5):
    print('')
    print(f"La maison du {i+1}er profil est {definitive_house[i][0][0]}")
    print('')
    for j in range(value_k_choice):
        print(f" --> Prénom du voisin n°{j+1} est : {kppv[i][j]['Name']}"
              f" et sa maison est : {kppv[i][j]['House']}")




    

